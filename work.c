#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>


char *argv[BUFSIZ];
char *spath[BUFSIZ];

void
error(void)
{
	char *error_msg = "An error occured";
	fprintf(stderr, "%s\n", error_msg);
}

char *
buildpath(char *path, int i)
{
	if (spath[i] == NULL) {
		return NULL;
	}

	char *token = "/";

	strlcpy(path, spath[i], BUFSIZ);	
	strlcat(path, token, BUFSIZ);
	strlcat(path, argv[0], BUFSIZ);

	return path;
}

int
path(void)
{
	int i = 0, j = 1;
	while (argv[j]) {
		spath[i++] = strdup(argv[j++]);
	} 

	return 0;
}

char *
checkpath(char *p)
{
	int i = 0;

	while ((buildpath(p, i++)) != NULL) {
		if (access(p, X_OK) == 0)
			return p;
	} 
	
	return NULL;
} 
		


int
execute()
{
	char path[BUFSIZ];
	
	if (checkpath(path) == NULL) {
		error();
		return -1;
	}
	
	int rc;
	
	if ((rc = fork()) == -1) {
		error();
		return -1;
	} else if (rc == 0)
		execv(path, argv);
	else
		wait(NULL);

	return 0;
}

int
echo(void)
{
	if (argv[1] != NULL) {
		if (strcmp(argv[1], "$$") == 0)  {
			fprintf(stdout, "%d\n", getpid());
			return 0;
		}
		
		write(STDOUT_FILENO, argv[1], strlen(argv[1]));
	}
	
	write(STDOUT_FILENO, "\n", sizeof(char));
	return 0;
}

	
int
cd(void)
{
	if (chdir(argv[1]) == -1) {
		error();
		return -1;
	} else
		return 0;
}
	

int
builtin(void)
{
	if (strcmp("cd", argv[0]) == 0)
		return 1;
	else if (strcmp("exit", argv[0]) == 0)
		return 2;
	else if (strcmp("path", argv[0]) == 0)
		return 3;
	else if (strcmp("echo", argv[0]) == 0)
		return 4;
	else
		return 5;
}


void
lexer(char *s)
{
	char **ap;

	for (ap = argv; ap < &argv[9] &&
	    (*ap = strsep(&s, " \t")) != NULL;) {
		if (**ap != '\0')
			++ap;
	}
	*ap = NULL;
}

int
checkinput(char *s)
{
	if (strcmp(s, "") == 0)
		return -1;

	while (*s) {
		if (!isspace(*s++))
			return 0;
	}

	return -1;
}


char *
getinput(char *s, FILE *f)
{
	if (f == stdin)
        	printf("rish&> ");

        if (fgets(s, BUFSIZ, f) != NULL) {
		s[strcspn(s, "\n")] = '\0';
		return s;
	}
	
	return NULL;
}             
