#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"

#include "rish.h"

#define BUFSIZE 1024

int init_suite(void) { return 0; }
int clean_suite(void) { return 0; }


void testLEXER(void)
{
	extern char *argv[BUFSIZE];
	char tstr[] = "cd /bin /bash/ /test /go";
	
	lexer(tstr);

	CU_ASSERT_STRING_EQUAL("cd", argv[0]);
	CU_ASSERT_STRING_EQUAL("/bin", argv[1]);
	CU_ASSERT_STRING_EQUAL("/bash/", argv[2]);
	CU_ASSERT_STRING_EQUAL("/test", argv[3]);
	CU_ASSERT_STRING_EQUAL("/go", argv[4]);
	CU_ASSERT_PTR_NULL(argv[5]);
}

void testBUILTIN(void)
{
	extern char *argv[BUFSIZE];
	char tstr[] = "exit";
	lexer(tstr);

	CU_ASSERT(2 == builtin());
}

void testBUILDPATH(void)
{
	extern char *argv[BUFSIZE];
	argv[0] = "ls";

	char p[] = "/bin";
	
	CU_ASSERT_STRING_EQUAL("/bin/ls", buildpath(p, 0));
}


int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", init_suite, clean_suite);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of lexer()", testLEXER)) || 
   (NULL == CU_add_test(pSuite, "test of buildpath()", testLEXER)) || 
      (NULL == CU_add_test(pSuite, "test of builtin()", testBUILTIN)))
 
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
