#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "rish.h"


int
main(int argc, char *argv[])
{
	FILE *f = stdin;
	extern char *spath[BUFSIZ];
	char buf[BUFSIZ];

	spath[0] = "/bin";

	if (argc > 2) {
		printf("usage ./rish for interactive mode"
		       " ./rish <file> for batch mode\n");
		exit(1);
	}
		

	if (argc == 2)
		f = fopen(argv[1], "r");
		

	while(1) {
		
		if (getinput(buf, f) == NULL) {
			fputs("\n", stdout);
			exit(1);
		}

		if (checkinput(buf) == -1)
			continue;

		lexer(buf);
		int c = builtin();

		switch(c) {
			case 1:
				cd();
				break;
			case 2:
				exit(0);
				break;
			case 3:
				path();
				break;
			case 4:
				echo();
				break;
			case 5:
				execute();
				break;
			default:
				break;
		}
	}

	return 0;
}
