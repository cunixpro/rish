CFLAGS= -Wall -Werror -Wextra -pedantic 



rish: main.c work.c
	cc ${CFLAGS} main.c work.c -o rish

test:
	cc -I/usr/local/include utests.c -o tests work.c -L/usr/local/lib -lcunit

clean:
	rm -f rish tests *.core
